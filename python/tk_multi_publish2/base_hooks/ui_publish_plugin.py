import sgtk
import os
from tank_vendor import six
from sgtk.platform.qt import QtCore, QtGui

HookBaseClass = sgtk.get_hook_baseclass()


class PublishUIPlugin(HookBaseClass):
    """
    Plugin for creating generic versions in Shotgun.

    """

    widget_source = None

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(self.disk_location, "icons", "create_version.png")

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Base Plugin UI Hook"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        return """
        Base Plugin UI Hook (Please subclass to implement functionality)
        """

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """

        return {}

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["*"]

    ############################################################################
    # standard publish plugin methods

    def create_settings_widget(self, parent, items=None, **kwargs):
        """
        Creates the widget for our plugin.
        :param parent: Parent widget for the settings widget.
        :type parent: :class:`QtGui.QWidget`
        :returns: Custom widget for this plugin.
        :rtype: :class:`QtGui.QWidget`
        """
        publish_item = items[0]

        # look for a named class in locals to check if unspecified
        # UI is present.

        if hasattr(self, "widget_source"):
            widget = self.widget_source
        # elif "PluginWidget" in locals().keys():
        #     widget = PluginWidget
        return widget(parent, publish_item=publish_item)

    def get_ui_settings(self, widget, items=None):
        """
        Retrieves the state of the ui and returns a settings dictionary.
        :param parent: The settings widget returned by :meth:`create_settings_widget`
        :type parent: :class:`QtGui.QWidget`
        :returns: Dictionary of settings.
        """
        pass
        # widget.publish_item = items[0]
        # widget.set_default_state()
        # return {"create_version": True}

    def set_ui_settings(self, widget, settings, items=None):
        """
        Populates the UI with the settings for the plugin.
        :param parent: The settings widget returned by :meth:`create_settings_widget`
        :type parent: :class:`QtGui.QWidget`
        :param list(dict) settings: List of settings dictionaries, one for each
            item in the publisher's selection.
        :raises NotImplementeError: Raised if this implementation does not
            support multi-selection.+
        """
        
        # if len(settings) > 1:
        #     raise NotImplementedError()
        # settings = settings[0]
        # widget.state = settings[self._SUBMIT_TO_FARM]
        pass
        # try:
        #     # plugin_config = settings_store.get(items[0].properties.get('ingest_file_config'))

        #     widget.publish_item = items[0]
        #     widget.clear_template_fields()
        #     widget.set_default_state()

        #     widget.pull_config()
        # except:
        #     self.logger.info(str(items))
        #     widget.text = str(items)

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A publish task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled
        """
        item.properties['logger'] = self.logger
        item.properties[ui_datastore_plugin_property] = {
            "id": str(uuid.uuid4()),
            "text1": str(uuid.uuid4())
        }
        path = item.properties.get("path")

        accepted = False

        if path:
            accept = True

            # lets store the verison name and make a flag for the attach_to_version plugin to catch.
            item.properties.version_name = self._get_version_name(path)
            item.properties.create_version = True

            # log the accepted file and display a button to reveal it in the fs
            self.logger.info("Create version plugin accepted: %s" % (path, ))
        else:
            self.logger.debug(
                "Create version plugin not accepted, 'path' was None")
        # return the accepted info
        return {"accepted": True}

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish.

        Returns a boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: True if item is valid, False otherwise.
        """

        # create a placeholder for finalize tasks
        item.properties.version_finalize = {"update": {}, "upload": {}}

        # start a count of attachments. This checked in the attach_to_version validation and
        # prevents a user from trying to attach more than 1 of each type to a version entry.
        item.properties.movies_attached = 0
        item.properties.frames_attached = 0
        item.properties.geo_attached = 0

        self.logger.info("A Version entry will be created in Shotgun: %s" %
                         (item.properties.version_name, ))
        self.logger.info(str(self.ui.field.text()))
        return True

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent
        path = item.properties["path"]
        version_name = item.properties["version_name"]
        first_frame = item.properties.get("first_frame")
        last_frame = item.properties.get("last_frame")

        self.logger.debug("Version name: %s" % (version_name, ))

        self.logger.info("Creating Version...")
        version_data = {
            "published_files": [item.properties.sg_publish_data],
            "project": item.context.project,
            "code": version_name,
            "description": item.description,
            "entity": self._get_version_entity(item),
            "sg_task": item.context.task,
            "sg_published": True,
        }

        if first_frame and last_frame:

            version_data["sg_first_frame"] = int(first_frame)
            version_data["sg_last_frame"] = int(last_frame)
            version_data["frame_range"] = "{}-{}".format(
                first_frame, last_frame)
            version_data["frame_count"] = int(last_frame - first_frame + 1)

        # log the version data for debugging
        self.logger.debug(
            "Populated Version data...",
            extra={
                "action_show_more_info": {
                    "label": "Version Data",
                    "tooltip": "Show the complete Version data dictionary",
                    "text": "<pre>%s</pre>" % (pprint.pformat(version_data), ),
                }
            },
        )

        # Create the version
        version = publisher.shotgun.create("Version", version_data)
        self.logger.info("Version created!")

        fw = self.load_framework("tk-framework-deadline_v0.x.x")

        output_reviewable = "/pipe/caches/log/from_publisher_01.mov"

        nxfw = self.load_framework("tk-framework-nx_v0.x.x")
        #ext = nxfw.import_module('ext')
        scalar = nxfw.import_module('scalar')
        # self.logger.error(str(ext.scalar))

        parent_task = None
        dispatcher = scalar.Dispatcher.using_queue('sgtk_deadline',
                                                   tk_framework_deadline=fw)

        upstream_scalar = item.parent.properties['upstream_scalar']
        if upstream_scalar:
            parent_task = upstream_scalar

            dispatcher = upstream_scalar.dispatcher

        t1 = None
        t2 = None

        # dispatcher.name = str(item.context.entity['name'])

        transcodify_template = self.sgtk.templates["transcodify_template"]
        #movs = ["mov", "m4v", "mp4", "mxf"]
        sg_transcodify_config = str(
            self.sgtk.paths_from_template(transcodify_template, {
                "ext": "mov",
                "name": "shotgun"
            })[0])

        if sg_transcodify_config:

            config_name = os.path.basename(
                os.path.splitext(sg_transcodify_config)[0])
            t1 = dispatcher.task(
                "transcodify",
                name="Creating Reviewable: {}".format(config_name),
                input_files="{}:{}-{}".format(path, first_frame, last_frame),
                output_file=output_reviewable,
                config_file=sg_transcodify_config,
                data={"sg_version": version},
                parent=parent_task)

            t2 = dispatcher.task("sg_upload",
                                 name="Uploading Movie to SG Version",
                                 entity_type=version.get('type'),
                                 entity_id=version.get('id'),
                                 src=output_reviewable,
                                 parent=t1)

            # process_output = dispatcher.process()
            #
            # self.logger.info(str(process_output))

            # associate this  as the last-created scalar task so you can either use it's dispatcher or make a child task
            item.parent.properties['upstream_scalar'] = t2

        # stash the version info in the item just in case
        item.properties["sg_version_data"] = version

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass
        # publisher = self.parent
        # thumb = item.get_thumbnail_as_path()
        # upload_thumb = True
        # version = item.properties["sg_version_data"]
        # finalize_tasks = item.properties.get("version_finalize")

        # if version and finalize_tasks:

        #     if finalize_tasks["update"]:
        #         publisher.shotgun.update("Version", version["id"], finalize_tasks["update"])

        #     if finalize_tasks["upload"]:
        #         for field, path in finalize_tasks["upload"].iteritems():
        #             self.logger.info("Uploading content...")

        #             # on windows, ensure the path is utf-8 encoded to avoid issues with
        #             # the shotgun api
        #             if sgtk.util.is_windows():
        #                 upload_path = six.ensure_text(path)
        #             else:
        #                 upload_path = path

        #             publisher.shotgun.upload(
        #                 "Version", version["id"], upload_path, field
        #             )
        #             upload_thumb = False

        # if upload_thumb:
        #     # only upload thumb if we are not uploading the content. with
        #     # uploaded content, the thumb is automatically extracted.
        #     self.logger.info("Uploading thumbnail...")
        #     publisher.shotgun.upload_thumbnail("Version", version["id"], thumb)

    def _get_version_name(self, path):
        """
        Returns the version name from a file path.

        :param path: The current path with a version number
        """
        # in Python3 this will become:
        # from pathlib import Path
        # return Path(path).stem

        return os.path.splitext(os.path.basename(path))[0]

    def _get_version_entity(self, item):
        """
        Returns the best entity to link the version to.
        """

        if item.context.entity:
            return item.context.entity
        elif item.context.project:
            return item.context.project
        else:
            return None